#!/usr/bin/env python
from rlcompleter import readline
readline.parse_and_bind("tab:complete")
 
import unittest
import os
import numpy as np
import sys

from imoose.kernel  import *
from imoose.solverutils      import *
from imoose.solver import pysolvers
#from imoose.solverutils.pySolverClasses.av3d import AV3D 
from imoose.solverutils.pySolverClasses.a3d import A3D 
#from imoose.solverutils.pySolverClasses.pgdAV3d import PGD_AV3D
from imoose.solverutils.excitations import A3D_Tpot
from imoose.solverutils.materialutils.materialproperties_scalar import MaterialProperty
import time
import math
#import pickle


modeldir  = '../modeldata'
treedir = '../utility'
materialdir = '../materialdata'
#readdir='/home/users/mueller/tmp/Fabian/TO_Ref'
meshname = '3DMagnetisierer_0_0033' # 'test_XX_3D_cylinder'
soludir = '../Solution_nonlinear'
name     = modeldir + os.sep + meshname
soluname = meshname
sigma=40e6 #Leitfaehigkeit alu
coilConduct =5.7e7
if not os.path.exists(soludir):
    os.makedirs(soludir)
order = 1
scale = 1.0
tau = 1.0
step= 0
frequenzy = 50.
periods=1.
nop=2.0
maxStep = int(nop*150)
deltaT = (nop/frequenzy)*periods/maxStep
Iw = 18000.0 #Stromdichte
verluste = np.zeros((maxStep, 1))
solveLinear=False
if solveLinear==False: solveNonLinear=True
D3=512
s   = pyfilehandlers.createFileHandlerStrategy(pyfilehandlers.ST_BASIC)
fh  = pyfilehandlers.FileHandler('Ansys60', s, True, D3)
fm  = fh.readMesh(name, order, scale, pyelem.MX)#MX,ED?

#s   = pyfilehandlers.createFileHandlerStrategy(pyfilehandlers.ST_BASIC)
#fh  = pyfilehandlers.FileHandler('Gmsh41', s, True, D3)
#fm  = fh.readMesh(name, order, scale, pyelem.ED)#MX,ED?

sfh = pyfilehandlers.SoluFileHandler(fm, 'Ansys', True)
petscInterface = pysolvers.PetscInterface([b'nuess'])#, '-ksp_monitor_true_residual'])
coilLabels = (3,)#?
#eddyCurrentLabels = (1,)#?
voltageLabels=() # weiß ich noch nicht genau wie das benutzt wird

nodeBoundaries = pyfem.BoundaryListBuilderNodeIdDouble(fm, (), (), {})
edgeBoundaries = pyfem.BoundaryListBuilderEdgeIdDouble(fm, (), (), {400:0.0, 401:0.0})

unitz = pygeo.GeoVec(0.0, 0.0, 0.0)
jDict = {}                 # Dictionary
brDict = {}
for i in fm.allLabels():
    jDict[i] = unitz       # Nullvektor
    brDict[i]=unitz
j = pystatic.ExciteLabelGeoVec(jDict)
br = pystatic.ExciteLabelGeoVec(brDict)
voltageJPV=pypost.PostVecDouble(fm) # Null Loesung
nuDict = {}
for i in fm.allLabels():
    nuDict[i] = '../materialdata/air.inds_relu'#fuer alles luft kommt das selbe fuer TO und A raus !
nuDict[1] = '../materialdata/team32_extrap.inds_relu'
nuDict[3]= '../materialdata/cu.inds_relu'
#muDict[7]  = '../materialdata/alu.inds_perm'
windingconfig={0: {3:{100: pygeo.GeoVec(0,Iw,0)}}} # Phase 0 : Material 3: Eintrittsflaeche 100 mit Stromvektor in y Richtung
windingCurrentMap = A3D_Tpot(sfh, fm, windingconfig, conduct = coilConduct, write=False, soluName = "./Solution/AV/3DMagnetisierer_0_0033_40e6_conduct_f_50/", step=int(step))
windingCurrentMap.generic_excitationcurrent_perPhase()
es= windingCurrentMap.windingCurrentMap[0][0]
del windingCurrentMap
actCo = pymaterial.MatPropConstDouble(sigma) #2 mal definieren?
linear=None; nonlinear=None; direction=None; f=None; frequency_set = {}; stress=None; cutedge=None; pm=None;
actNu = MaterialProperty(fm,nuDict,linear,nonlinear,direction, f,frequency_set, stress,cutedge,pm)
oldNu = actNu; oldCo=actCo;
#eddyMesh = fm.getSubMesh(eddyCurrentLabels,"eddyMesh")
verlust=list()
#fh.writeMesh("EddyMesh033",eddyMesh)
for step in range(0, maxStep):
    # first create Current Excitation Potential
    currentEs = pypost.EdgeSoluDouble()
    #currentExcitationLabel = tuple()
    currentEs.insertValsFromUpper(es*np.sin(2*np.pi*frequenzy*deltaT*step)) # add T of phase to EdgeSolu
    
    actSoluT = currentEs  
    if step==0: oldSoluT = actSoluT 
    femProb = A3D(fm, nodeBoundaries.get(), edgeBoundaries.get(), coilLabels, voltageLabels,deltaT, tau,b'MINRES', b'CHOLESKY',order) #hier auf A3D umstellen
    if step==0:femProb.solveTransient= False
    else: femProb.solveTransient=False
    femProb.setMaterialsPy(actNu,oldNu, actCo,oldCo)
    # j and br are constant in this simulation
    femProb.setExcitations(br, br, j, j, actSoluT, oldSoluT,voltageJPV,voltageJPV) # ich glaube die Funktion gibt es bei A3D nicht
    print("Before setOld")
    if step==0:#NullSoluV = femProb.buildNullSoluV(); 
        pass#NullSoluA = femProb.buildNullSoluA();#femProb.setOldSolutions(NullSoluA,NullSoluV); # NullSoluV gibt es auch nicht
    else: #femProb.setOldSolutionsPy(oldSoluA)#, oldSoluV) # setOldSolutionsPy gibt es so auch nicht, muesste aber prinzipiell nix anderes machen als femprob.previousSolution = oldSoluA
        femProb.setPreviousSolution(oldSoluA)
    print("After Set Solu")
    nrIteration = iterationschemes.NewtonRaphsonIteration(femProb)
    nrIteration.cGTolerance = 1e-7#1e-7?
    nrIteration.nLTolerance = 5e-4
    nrIteration.aTolerance = 1e-9
    if solveLinear==True: nrIteration.solveLinearSystem()
    elif solveNonLinear==True: nrIteration.solveNonLinearSystem()
    #soluV = femProb.getActNodeSolution() #bei A3D heißt die einzige Funktion die man braucht getEdgeSolution(). KnotenLoesungen existieren nicht
    #sfh.writeSolu(soludir+os.sep+meshname+"_AV_V"'.%04d' % step, soluV)
    #
    soluA = femProb.getEdgeSolution()
    sfh.writeSolu(soludir+os.sep+meshname+"_AV_A"'.%04d' % step, soluA)
    fluxDensity  = femProb.getFluxdensity()
    sfh.writeSoluMidValue(soludir+os.sep+meshname+"_AV_B"+'.%04d' % step, fluxDensity)
    #if step>0:eddyDens = femProb.getEddyCurrentDensity(eddyMesh, oldSoluA, oldSoluV, actCo)
    #else: eddyDens = pypost.PostVecDouble(eddyMesh)
    #sfh.writeSoluMidValue(soludir+os.sep+meshname+"_AV_J"+'.%04d' % step, eddyDens)
    #squared=eddyDens.square()
    #integral=squared.integrate()
    #verlust.append(integral/sigma)
    oldNu = actNu; oldCondu=actCo;
    actNu = MaterialProperty(fm,nuDict,linear,nonlinear,direction, f,frequency_set, stress,cutedge,pm)
    actCo = pymaterial.MatPropConstDouble(sigma)
    oldSoluA = soluA; #oldSoluV=soluV.subSolution(eddyMesh)#pypost.NodeSoluDouble(soluV)*1.0;
    oldSoluA =oldSoluA*1.0; 
    del nrIteration
    del femProb
fobj_out='1000A_AV_Referenz_Losses_Cu-'+str(sigma)
timeend=nop*(1.0/frequenzy)
np.savetxt(fobj_out,verlust,fmt='%1.15f')


