import unittest
import os
import numpy as np
import sys
import pickle
import time
start = time.time()

from imoose.kernel.pymoose   import *
from imoose.solver.pysolvers import pystatram3d
from imoose.solver.pysolvers import pyedgesolver
from imoose.solver.pysolvers import pyt_omega3d
from imoose.solverutils      import *
from imoose.solver.pysolvers import *

#datadir  = '..'
#meshname = 'full_nonconformal_iemklein3d'
datadir  = '.'
meshname = 'full_nonconf_poor3d'
name     = datadir + os.sep + meshname
soluname = meshname
order = 1
scale = 1.0
s   = pyfilehandlers.createFileHandlerStrategy(pyfilehandlers.ST_MAP)
fh  = pyfilehandlers.FileHandler('Ansys60', s)
fm  = fh.readMesh(name, order, scale, pyelem.MX)
sfh = pyfilehandlers.SoluFileHandler(fm, 'Ansys', 1)
cu  = pymaterial.MatPropConstDouble(4.73e7)

petscInterface = PetscInterface(['nuess', '-ksp_monitor_true_residual'])

#otherslabel = [ 2, 4, 5, 6, 28, 29, 30, 32, 220, 234, 235, 236 ]
#otherslabel = [ 2, 304, 404, 504, 604, 305, 405, 505, 605, 306, 406, 506, 606, 28, 29, 30, 32, 220, 234, 235, 236 ]
#otherslabel = [ 2, 4, 5, 6, 28, 32, 220, 234, 235, 236 ] # fail
otherslabel = [] # ok
#otherslabel = [32,] # ok
statorlabel = list( set(fm.allLabels()).difference( set(otherslabel) ))
es = pypost.EdgeSoluDouble()

statorfm = fm.getSubMesh( statorlabel, "stator" )
othersfm = fm.getSubMesh( otherslabel, "others" )

edges = set()
cEIter = pyelem.ConstFemMeshElemIter(othersfm)
cEIter.First()
while cEIter.NFUp():
    elem = cEIter.Item()
    fieldEdgeList = elem.fieldEdgeList()
    for localEdge in range(1, fieldEdgeList.size() + 1):
        edge = fieldEdgeList[localEdge]
        edges.add( edge )
        es.insert( edge, 0.0 )
    cEIter.Next()

edgesFm = set()
cEIter = pyelem.ConstFemMeshElemIter(fm)
cEIter.First()
while cEIter.NFUp():
    elem = cEIter.Item()
    fieldEdgeList = elem.fieldEdgeList()
    for localEdge in range(1, fieldEdgeList.size() + 1):
        edge = fieldEdgeList[localEdge]
        edgesFm.add( edge )
    cEIter.Next()

edgesStator = set()
cEIter = pyelem.ConstFemMeshElemIter(statorfm)
cEIter.First()
while cEIter.NFUp():
    elem = cEIter.Item()
    fieldEdgeList = elem.fieldEdgeList()
    for localEdge in range(1, fieldEdgeList.size() + 1):
        edge = fieldEdgeList[localEdge]
        edgesStator.add( edge )
    cEIter.Next()

edgesOthers = set()
cEIter = pyelem.ConstFemMeshElemIter(othersfm)
cEIter.First()
while cEIter.NFUp():
    elem = cEIter.Item()
    fieldEdgeList = elem.fieldEdgeList()
    for localEdge in range(1, fieldEdgeList.size() + 1):
        edge = fieldEdgeList[localEdge]
        edgesOthers.add( edge )
    cEIter.Next()


print "before init"
print len(edges)

#phases = {1: { 3  : {103: pygeo.GeoVec(0., 0., -64.0)},
#               81 : {181: pygeo.GeoVec(0., 0., 64.0)},
#               6  : {106: pygeo.GeoVec(0., 0., -64.0)},
#               84 : {184: pygeo.GeoVec(0., 0., 64.0)}
#             },
#		  2: { 4  : {104: pygeo.GeoVec(0., 0., 98.0)},
#               79 : {179: pygeo.GeoVec(0., 0., -98.0)},
#               7  : {107: pygeo.GeoVec(0., 0., 98.0)},
#               82 : {182: pygeo.GeoVec(0., 0., -98.0)}},
#		  3: { 5  : {105: pygeo.GeoVec(0., 0., -34.0)},
#               80 : {180: pygeo.GeoVec(0., 0.,  34.0)},
#               8  : {108: pygeo.GeoVec(0., 0., -34.0)},
#               83 : {183: pygeo.GeoVec(0., 0., 34.0)},
#             }
#		  }
currentDict = { 3  : {103: pygeo.GeoVec(0., 0., -64.0)},
               81 : {181: pygeo.GeoVec(0., 0., 64.0)},
               6  : {106: pygeo.GeoVec(0., 0., -64.0)},
               84 : {184: pygeo.GeoVec(0., 0., 64.0)},
		       4  : {104: pygeo.GeoVec(0., 0., 98.0)},
               79 : {179: pygeo.GeoVec(0., 0., -98.0)},
               7  : {107: pygeo.GeoVec(0., 0., 98.0)},
               82 : {182: pygeo.GeoVec(0., 0., -98.0)},
		       5  : {105: pygeo.GeoVec(0., 0., -34.0)},
               80 : {180: pygeo.GeoVec(0., 0.,  34.0)},
               8  : {108: pygeo.GeoVec(0., 0., -34.0)},
               83 : {183: pygeo.GeoVec(0., 0., 34.0)},
             }


areaList = []
condLabel = set()
#for phase in phases.values():
#  for v in phase.values():
#    areaList.append( v.keys()[0] )
#    areaList.append( v.keys()[0] + 100 )
for v in currentDict.values():
  areaList.append( v.keys()[0] )
  areaList.append( v.keys()[0] + 100 )

for label in currentDict.keys():
  condLabel.add(label)
#for phase in phases.values():
#  for label in phase.keys():
#    condLabel.add(label)

tp = tpotentialalgebraicboundaries.tPotentialAlgebraicBoundaries(statorfm, currentDict , areaList, condLabel = condLabel )

tp.setup()
tp.sortEdges()
tp.buildSpanningTree()
#f = open('spannbaum.dat', 'w')
#pickle.dump(tp.Ltree, f)
#f.close()
tp.fixSurface()


print "after fixSurface ", time.time() - start
start = time.time()

# get edges which numerical value has to be evaluated
edgesToSet = set()
for edge in tp.fixedEdges:
    if not tp.myes.value(edge).isValueSet:
        edgesToSet.add(edge)

print "after edgesToSet ", time.time() - start
start = time.time()

# get faces with at least one edge from   edgesToSet
facesEquation = set()
cEIter = pyelem.ConstFemMeshElemIter(tp.lfm[0])
cEIter.First()
while cEIter.NFUp():
    elem = cEIter.Item()
    surfacesToFix = range(1,5)
    for surface in surfacesToFix:
        nodes = elem.nodesOfSide(surface)
        nodes.sort()
        edges = [ (nodes[1], nodes[2]), (nodes[1], nodes[3]), (nodes[2], nodes[3]) ]
        for edge in edges:
            if edge in edgesToSet:
                facesEquation.add( (nodes[1], nodes[2], nodes[3]) )
                break
    cEIter.Next()

print "after faceEquation ", time.time() - start
start = time.time()

# get equations
fieldEdgeDirection = [tpotentialalgebraicboundaries.ExtNumber(1.0), \
                      tpotentialalgebraicboundaries.ExtNumber(-1.0), \
                      tpotentialalgebraicboundaries.ExtNumber(1.0)]
equations = set()
for face in facesEquation: # face = (node1, node2, node3)
    value = tpotentialalgebraicboundaries.ExtNumber(0.0)
    edges = [ (face[0], face[1]), (face[0], face[2]), (face[1], face[2]) ]
    for edge in edges:
        value = value +  fieldEdgeDirection[edges.index(edge)] * tp.myes.value(edge)
    
#    if set(value.getSymbolicVarList()) != set([0.0]): # do not add trivial equations
    if (len(value.getSymbolicVarList()) > 1) or (set(value.getSymbolicVarList()) != set([0.0])): # do not add trivial equations
        eq = (value.value, tuple(value.getSymbolicVarList()) )
        equations.add( eq )
#    for i in range(tpotentialalgebraicboundaries.ExtNumber.numberOfSymVars):
#
#        if value.getSymbolicVar(i) != 0.0:
#            eq = (value.value, tuple(value.getSymbolicVarList()) )
##            if eq not in equations:
##                print value
#            equations.add( eq )
#            break

print "after equations ", time.time() - start
start = time.time()

# solve equations
from scipy.sparse.linalg.dsolve import linsolve
import scipy.sparse as sps
import numpy as np
import scipy

dofs = tpotentialalgebraicboundaries.ExtNumber.numberOfSymVars
print "DoFs: ", dofs
print "Equations: ", len(equations)
# method 1 : sparse
Amat = sps.lil_matrix( (dofs, dofs) )
bvec = np.zeros(dofs)

row = 0
for eq in equations:
    bvec[row] -= eq[0]
    for i in range(dofs):
        Amat[row, i] += eq[1][i]
    row = (row + 1) % dofs

Amat = Amat.tocsr()
xvec = linsolve.spsolve(Amat, bvec)
#print Amat * xvec - bvec

## method 2 : dense
#Amat = np.zeros( (dofs, dofs) )
#bvec = np.zeros(dofs)
#
#row = 0
#for eq in equations:
#    bvec[row] -= eq[0]
#    for i in range(dofs):
#        Amat[row, i] += eq[1][i]
#    row = (row + 1) % dofs
#
#xvec = scipy.linalg.solve(Amat, bvec)
#print np.dot(Amat,xvec) - bvec

xvec = np.round(xvec) # TODO: always integer?

# set edges in tp.myes
for edge in edgesToSet:
    enumber = tp.myes.value(edge)
    enumber.value += np.dot(np.array(enumber.getSymbolicVarList()), xvec)
    enumber.isValueSet = True



es = pypost.EdgeSoluDouble()
for edge in tp.fixedEdges:
    if not tp.myes.value(edge).isValueSet:
        raise
    else:
        es.insert( edge, tp.myes.value(edge).value )

print "after solve equations ", time.time() - start
start = time.time()

tp.es = es


tp.fixVolume(tol = 1e-14)

sfh = pyfilehandlers.SoluFileHandler(tp.fm, 'Ansys', 1)
#sfh.writeSoluMidValue(meshname + '_orig_J', tp.getCurrentDensity())
#sfh.writeSolu(meshname + '_orig', tp.es)


# TODO Begin

esnew = pypost.EdgeSoluDouble()
for edge in edgesOthers:
  esnew.insert( edge, 0.0 )

for edge in edgesStator:
  esnew.insert( edge, tp.es.value(edge) )

postedge = pypost.PostEdgeDouble(fm)
postedge.fromFemSolu(esnew, 1)
#sfh.writeSoluMidValue(meshname + '_phase_' + str(whichPhase) +'_J', postedge.curl())
#sfh.writeSolu(meshname + '_phase_' + str(whichPhase) + '_T', esnew)

sfh.writeSoluMidValue(meshname + '_full_J', postedge.curl())
sfh.writeSolu(meshname + '_full_T', esnew)
# TODO End


raise




boundaries = pyfem.BoundaryListBuilderNodeIdDouble(fm, (), (), {1: 0., 2: 0., 3: 0., 4: 0.})
#boundaries = pyfem.BoundaryListBuilderNodeIdDouble(fm, (), (), {})

class TOmega3D(pyt_omega3d.statramTOmega3dPetsc):
    def __init__(self, mesh, boundarylist, solver, preconditioner):
        pyt_omega3d.statramTOmega3dPetsc.__init__(
            self, mesh, boundarylist, solver, preconditioner)
        self.__mesh = mesh
        #self.__ps   = pypost.PostScalDouble(mesh)
        self.__h    = pypost.PostVecDouble(mesh)
    def prepareMaterialProperties(self):
        self.__h  = self.getMagneticField(self.getNodeSolution())
        #self.__ps = self.__h.square()
        #self.mu.setResultAttr(self.__ps)
        self.mu.setResultAttr(self.__h)
    def getMagneticField(self, solu):
        _hPE = pypost.PostEdgeDouble(self.__mesh)
        _t0 = pypost.PostVecDouble(self.__mesh)
        _hPE.fromFemSolu(self.t0, 1)
        _t0.midValsFromPostEdge(_hPE)
        _ps = pypost.PostScalDouble(self.__mesh)
        _ps.fromFemSolu(solu, 1)
        return _t0 - _ps.grad()

#brPV = pypost.PostVecDouble(fm)
#cElemIter = pyelem.FemMeshElemIter(fm)
#cElemIter.First()
#while cElemIter.NFUp():
#    elem = cElemIter.Item()
#    elemSolu = brPV.getElemSoluNonConst(elem.getId())
#    value = br.value(elem)
#    elemSolu.setMidValue(pyvecmat.NVectorDouble(value[1], value[2], value[3]))
#    cElemIter.Next()

# material property
class MyConstProperties(pymaterial.MatPropDouble):
    def __init__(self):
        pymaterial.MatPropDouble.__init__(self)
    def isConst(self, elem):
        return True
    def value(self, elem, coord):
        mu_r = 1.0
        if elem.label() in (2,31):
            mu_r = 1000
        return 4.0e-7*np.pi*mu_r
    def derivative(self, elem, coord):
        return 0.
    def setResultAttr(self, pe):
        pass

mu_l = MyConstProperties()

femprob = TOmega3D(fm, boundaries.get(), 'CG', 'CHOLESKY')
#femprob = TOmega3D(fm, boundaries.get(), 'CG', 'SSOR')
#femprob.br = br
femprob.mu = mu_l
#femprob.t0 = 0.5 * es
#femprob.t0 = es
femprob.t0 = esnew
#femprob.setCoilLabels(fm.allLabels())
nrIteration = iterationschemes.NewtonRaphsonIteration(femprob)
nrIteration.cGTolerance = 1e-14
#nrIteration.nLTolerance = 1e-4
#nrIteration.aTolerance = 1e-8
#nrIteration.solveNonLinearSystem()

print "\n\n Before solveLinearSystem \n\n"

nrIteration.solveLinearSystem()

print "\n\n After solveLinearSystem \n\n"

ns = femprob.getNodeSolution()

sfh = pyfilehandlers.SoluFileHandler(fm, 'Ansys', 1)
sfh.writeSolu(soluname, ns)

fluxDensity  = femprob.getMagneticField(ns)
fluxDensity *= mu_l
sfh.writeSoluMidValue('bfeld', fluxDensity)

#f = globaltorqueandforces.GlobalForceFromNodalForces(
#  (1, ), (2, ), axis = pygeo.GeoVec(0., 1., 0.))







raise # TODO


hPE = pypost.PostEdgeDouble(fm)
hPV = pypost.PostVecDouble(fm)
#hPE.fromFemSolu(tp.es, 1)
hPE.fromFemSolu(esnew, 1)
hPV.midValsFromPostEdge(hPE)
hPV = 4.0e-7 * np.pi * hPV

class h_exc(pystatic.ExciteGeoVec):
    def __init__(self):
        pystatic.ExciteGeoVec.__init__(self)
        self.__center = pyvecmat.VectorCDouble(4)
        self.__center[1] = 0.3
        self.__center[2] = 0.3
        self.__center[3] = 0.3
        self.__center[4] = 0.3
    def value(self, elem):
        return hPV.value(elem, self.__center)

h = h_exc()

#brPV = pypost.PostVecDouble(fm)
#cElemIter = pyelem.FemMeshElemIter(fm)
#cElemIter.First()
#while cElemIter.NFUp():
#    elem = cElemIter.Item()
#    elemSolu = brPV.getElemSoluNonConst(elem.getId())
#    value = br.value(elem)
#    elemSolu.setMidValue(pyvecmat.NVectorDouble(value[1], value[2], value[3]))
#    cElemIter.Next()

# material property
class MyConstProperties(pymaterial.MatPropDouble):
    def __init__(self):
        pymaterial.MatPropDouble.__init__(self)
    def isConst(self, elem):
        return True
    def value(self, elem, coord):
        mu_r = 1.0
        if elem.label() in (2,6):
            mu_r = 1000
        return 4.0e-7*np.pi*mu_r
    def derivative(self, elem, coord):
        return 0.
    def setResultAttr(self, pe):
        pass

fm  = fh.readMesh(name, order, scale, pyelem.NO)

femprob = pyt_omega3d.statramTOmega3dPetsc(fm, boundaries.get(), 'CG', 'CHOLESKY')
femprob.br = h
femprob.mu = mu
nrIteration = iterationschemes.NewtonRaphsonIteration(femprob)
#nrIteration.cGTolerance = 1e-40
nrIteration.cGTolerance = 1e-12
nrIteration.solveLinearSystem()

ns = femprob.getNodeSolution()

sfh = pyfilehandlers.SoluFileHandler(fm, 'Ansys', 1)
sfh.writeSolu(soluname, ns)































