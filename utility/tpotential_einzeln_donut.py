import unittest
import os
import numpy as np
import sys
import pickle
import time
start = time.time()

from imoose.kernel.pymoose   import *
from imoose.solver.pysolvers import pystatram3d
from imoose.solver.pysolvers import pyedgesolver
from imoose.solver.pysolvers import pyt_omega3d
from imoose.solverutils      import *
from imoose.solver.pysolvers import *

datadir  = '../modeldata'
meshname = '3DMagnetisierer'
name     = datadir + os.sep + meshname
soluname = meshname
order = 1
scale = 1.0
s   = pyfilehandlers.createFileHandlerStrategy(pyfilehandlers.ST_MAP)
fh  = pyfilehandlers.FileHandler('Ansys60', s)
fm  = fh.readMesh(name, order, scale, pyelem.MX)
sfh = pyfilehandlers.SoluFileHandler(fm, 'Ansys', 1)
cu  = pymaterial.MatPropConstDouble(4.73e7)

petscInterface = PetscInterface(['nuess', '-ksp_monitor_true_residual'])

es = pypost.EdgeSoluDouble()

edges = set()
cEIter = pyelem.ConstFemMeshElemIter(fm)
cEIter.First()
while cEIter.NFUp():
    elem = cEIter.Item()
    fieldEdgeList = elem.fieldEdgeList()
    for localEdge in range(1, fieldEdgeList.size() + 1):
        edge = fieldEdgeList[localEdge]
        edges.add( edge )
    cEIter.Next()

edgesFix = set()

pyt_omega3d.statramT3dPetsc(fm, 1, 10, pygeo.GeoVec(0,1,0), (10,), (1,), pyfem.BoundaryListEdgeIdDouble() ,'CG', 'CHOLESKY')


# init
tpotentialalgebraicboundaries.ExtNumber.numberOfSymVars = 0
tp = tpotentialalgebraicboundaries.tPotentialAlgebraicBoundaries(fm, {3 : {100: pygeo.GeoVec(0, 1., 0.)}}, [100,], [3,], fixedEdges = edgesFix , es = es)
    
tp.setup()
tp.sortEdges()
tp.buildSpanningTree()
#f = open('spannbaum.dat', 'w')
#pickle.dump(tp.Ltree, f)
#f.close()
tp.fixSurface()


print "after fixSurface ", time.time() - start
start = time.time()

# get edges which numerical value has to be evaluated
edgesToSet = set()
for edge in tp.fixedEdges:
    if not tp.myes.value(edge).isValueSet:
        edgesToSet.add(edge)

es = pypost.EdgeSoluDouble()
for edge in tp.fixedEdges:
    if not tp.myes.value(edge).isValueSet:
        raise
    else:
        es.insert( edge, tp.myes.value(edge).value )

print "after solve equations ", time.time() - start
start = time.time()

tp.es = es


tp.fixVolume(tol = 1e-14)

sfh = pyfilehandlers.SoluFileHandler(tp.fm, 'Ansys', 1)
#sfh.writeSoluMidValue(meshname + '_orig_J', tp.getCurrentDensity())
#sfh.writeSolu(meshname + '_orig', tp.es)


# TODO Begin

esnew = pypost.EdgeSoluDouble()

for edge in edges:
  esnew.insert( edge, tp.es.value(edge) )

postedge = pypost.PostEdgeDouble(fm)
postedge.fromFemSolu(esnew, 1)
sfh.writeSoluMidValue(meshname + '_J', postedge.curl())
sfh.writeSolu(meshname + '_T', esnew)

# TODO End
