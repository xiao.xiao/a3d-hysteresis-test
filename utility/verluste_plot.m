close all
soludir = "/storage/users/mueller/MOR/ISEF/PGD/Joule_loesungen/40e6_0.0033/";
file_ref = fopen("/storage/users/mueller/MOR/ISEF/Reference/3DMagnetisierer_0_0033_40e6_conduct_f_50/" + "1000A_T_Omega_Referenz");
%file_ref = fopen(soludir + "testVerlust_ref");
file_mode1 = fopen(soludir + "testVerlust_1Modes");
file_mode2 = fopen(soludir + "testVerlust_2Modes");
file_mode3 = fopen(soludir + "testVerlust_3Modes");
file_mode4 = fopen(soludir + "testVerlust_4Modes");
file_mode6 = fopen(soludir + "testVerlust_6Modes");

verlust_ref = fscanf(file_ref,'%f');
verlust_mode1 = fscanf(file_mode1,'%f');
verlust_mode2 = fscanf(file_mode2,'%f');
verlust_mode3 = fscanf(file_mode3,'%f');
verlust_mode4 = fscanf(file_mode4,'%f');
verlust_mode6 = fscanf(file_mode6,'%f');
fighandle = figure;
set(fighandle, 'Position', [200, 300, 1120, 840]);
%plot([verlust_ref, verlust_mode1, verlust_mode2, verlust_mode3, verlust_mode4])
%figure()
plot(verlust_ref(1:300),'-x');
hold all
plot(verlust_mode1(1:300))
plot(verlust_mode2(1:300),'o')
plot(verlust_mode3(1:300),'diamond')
plot(verlust_mode4(1:300),'--o')
plot(verlust_mode6(1:300),'*')
eps=norm(verlust_mode1(1:300)-verlust_ref)/norm(verlust_ref);
eps2=norm(verlust_mode2(1:300)-verlust_ref(1:300))/norm(verlust_ref(1:300));
eps3=norm(verlust_mode3(1:300)-verlust_ref(1:300))/norm(verlust_ref(1:300));
eps4=norm(verlust_mode4(1:300)-verlust_ref(1:300))/norm(verlust_ref(1:300));
eps6=norm(verlust_mode6(1:300)-verlust_ref(1:300))/norm(verlust_ref(1:300));
mu_0=4*pi*1e-7;
delta=sqrt(2/(2*pi*50*40e6*mu_0));
mesh_size=delta/3;

title('Verluste');
xlabel('Zeitschritt');
ylabel('Wirbelstromverluste');
legend('Referenz','1Mode','2Moden','3Moden','4Moden')