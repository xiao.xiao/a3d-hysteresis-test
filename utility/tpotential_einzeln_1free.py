# TODO End
import unittest
import os
import numpy as np
import sys
import pickle
import time
start = time.time()

from imoose.kernel.pymoose   import *
from imoose.solver.pysolvers import pystatram3d
from imoose.solver.pysolvers import pyedgesolver
from imoose.solver.pysolvers import pyt_omega3d
from imoose.solverutils      import *
from imoose.solver.pysolvers import *
#Richtige Geometrie
datadir  = '../modeldata'
#meshname = 'induktionsofen_neumann'
meshname = '3DMagnetisierer_0_0033'
name     = datadir + os.sep + meshname
soluname = meshname
order = 1
scale = 1.0
s   = pyfilehandlers.createFileHandlerStrategy(pyfilehandlers.ST_MAP)
fh  = pyfilehandlers.FileHandler('Ansys60', s)
fm  = fh.readMesh(name, order, scale, pyelem.MX)
sfh = pyfilehandlers.SoluFileHandler(fm, 'Ansys', 1)
cu  = pymaterial.MatPropConstDouble(4.73e7)

petscInterface = PetscInterface(['nuess', '-ksp_monitor_true_residual'])

es = pypost.EdgeSoluDouble()

edges = set()
edgesfm=set()
cEIter = pyelem.ConstFemMeshElemIter(fm)
cEIter.First()
while cEIter.NFUp():
    elem = cEIter.Item()
    fieldEdgeList = elem.fieldEdgeList()
    for localEdge in range(1, fieldEdgeList.size() + 1):
        edge = fieldEdgeList[localEdge]
        edges.add( edge )
        edgesfm.add(edge)
    cEIter.Next()

edgesFix = set()
print len(edgesfm)
#pyt_omega3d.statramT3dPetsc(fm, 1, 10, pygeo.GeoVec(0,1,0), (10,), (1,), pyfem.BoundaryListEdgeIdDouble() ,'CG', 'CHOLESKY')


# init
tpotentialalgebraicboundaries.ExtNumber.numberOfSymVars = 0
tp = tpotentialalgebraicboundaries.tPotentialAlgebraicBoundaries(fm, {3 : {100: pygeo.GeoVec(0., 1., 0.)}}, [100,], [3,], fixedEdges = edgesFix , es = es)
tp.setup()
tp.sortEdges()
tp.buildSpanningTree()
tp.fixSurface()


print "after fixSurface ", time.time() - start
start = time.time()

# get edges which numerical value has to be evaluated
edgesToSet = set()
for edge in tp.fixedEdges:
    if not tp.myes.value(edge).isValueSet:
        print edge
        edgesToSet.add(edge)

print "after edgesToSet ", time.time() - start
start = time.time()

# get faces with at least one edge from   edgesToSet
facesEquation = set()
cEIter = pyelem.ConstFemMeshElemIter(tp.lfm[0])
cEIter.First()
while cEIter.NFUp():
    elem = cEIter.Item()
    surfacesToFix = range(1,5) # welche sind das? Leiteroberflachen? Dann ist zumindest die 3 richtig
    for surface in surfacesToFix:
        nodes = elem.nodesOfSide(surface)
        nodes.sort()
        edges = [ (nodes[1], nodes[2]), (nodes[1], nodes[3]), (nodes[2], nodes[3]) ]
        for edge in edges:
            if edge in edgesToSet:
                facesEquation.add( (nodes[1], nodes[2], nodes[3]) )
                break
    cEIter.Next()

print "after faceEquation ", time.time() - start
start = time.time()

# get equations
fieldEdgeDirection = [tpotentialalgebraicboundaries.ExtNumber(1.0), \
                      tpotentialalgebraicboundaries.ExtNumber(-1.0), \
                      tpotentialalgebraicboundaries.ExtNumber(1.0)]
equations = set()
for face in facesEquation: # face = (node1, node2, node3)
    value = tpotentialalgebraicboundaries.ExtNumber(0.0)
    edges = [ (face[0], face[1]), (face[0], face[2]), (face[1], face[2]) ]
    for edge in edges:
        value = value +  fieldEdgeDirection[edges.index(edge)] * tp.myes.value(edge)
    
    if (len(value.getSymbolicVarList()) > 1) or (set(value.getSymbolicVarList()) != set([0.0])): # do not add trivial equations
        eq = (value.value, tuple(value.getSymbolicVarList()) )
        equations.add( eq )


print "after equations ", time.time() - start
start = time.time()

# solve equations
from scipy.sparse.linalg.dsolve import linsolve
import scipy.sparse as sps
import numpy as np
import scipy

dofs = tpotentialalgebraicboundaries.ExtNumber.numberOfSymVars
print "DoFs: ", dofs
print "Equations: ", len(equations)
#if len(equations)>0:
# method 1 : sparse
Amat = sps.lil_matrix( (dofs, dofs) )
bvec = np.zeros(dofs)

row = 0
for eq in equations:
    bvec[row] -= eq[0]
    for i in range(dofs):
        Amat[row, i] += eq[1][i]
    row = (row + 1) % dofs

Amat = Amat.tocsr()
xvec = linsolve.spsolve(Amat, bvec)   
xvec = np.round(xvec) # TODO: always integer?

# set edges in tp.myes
for edge in edgesToSet:
    print edge
    enumber = tp.myes.value(edge)
    enumber.value += np.dot(np.array(enumber.getSymbolicVarList()), xvec)
    enumber.isValueSet = True



es = pypost.EdgeSoluDouble()
for edge in tp.fixedEdges:
    if not tp.myes.value(edge).isValueSet:
        raise
    else:
        es.insert( edge, tp.myes.value(edge).value )

print "after solve equations ", time.time() - start
start = time.time()

tp.es = es


tp.fixVolume(tol = 1e-14)

sfh = pyfilehandlers.SoluFileHandler(tp.fm, 'Ansys', 1)


#-------------------------------------------------------------------------------

# TODO Begin

esnew = pypost.EdgeSoluDouble()

for edge in edgesfm:
  esnew.insert( edge, tp.es.value(edge) )

postedge = pypost.PostEdgeDouble(fm)
postedge.fromFemSolu(esnew, 1)
sfh.writeSoluMidValue(meshname + '_J', postedge.curl())
print 'Write T'
sfh.writeSolu(meshname + '_T', esnew)

# TODO End
